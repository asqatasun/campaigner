# Campaigner

## Usage

Campaigner is designed to run accessibility audits on very large number of URLs.

We assume:

- URLs are stored in a plain-text file, with one URL per line
- The URL file is spliced in files containing 1000 URLs (i.e. 1000 lines). The Unix command `split` does a great job for
  this.