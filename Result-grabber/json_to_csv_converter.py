import csv
import json
import sys
from datetime import datetime


def extract_info(item):
    url = item['subject']['url']
    date = item['date']
    # Convertir la date au format souhaité
    date = datetime.fromisoformat(date.replace("Z", "+00:00")).strftime("%Y-%m-%d %H:%M:%S")
    status = item['status']

    # Initialiser les valeurs par défaut
    errors = tests_non_applicables = tests_conformes = tests_non_conformes = tests_pre_qualifies = tests_non_testes = grade = ""

    if status != "ERROR":
        grade = item['subject']['grade']
        # Nombre d'erreurs
        errors = item['subject']['nbOfFailureOccurrences']

        # Extraire les informations de 'repartitionBySolutionType'
        for entry in item['subject']['repartitionBySolutionType']:
            if entry['type'] == 'PASSED':
                tests_conformes = entry['number']
            elif entry['type'] == 'FAILED':
                tests_non_conformes = entry['number']
            elif entry['type'] == 'NEED_MORE_INFO':
                tests_pre_qualifies = entry['number']
            elif entry['type'] == 'NOT_APPLICABLE':
                tests_non_applicables = entry['number']
            elif entry['type'] == 'NOT_TESTED':
                tests_non_testes = entry['number']

    return [url, date, status, errors, tests_non_applicables, tests_conformes, tests_non_conformes, tests_pre_qualifies,
            tests_non_testes, grade]


def main():
    # Vérifier si les chemins des fichiers JSON et CSV ont été fournis en argument
    if len(sys.argv) != 3:
        print("Usage: python convert_json_to_csv.py <path_to_json_file> <path_to_output_csv_file>")
        sys.exit(1)

    # Lire les chemins des fichiers JSON et CSV depuis les arguments
    input_file = sys.argv[1]
    output_file = sys.argv[2]

    # Lire le JSON à partir du fichier
    with open(input_file, 'r') as json_file:
        data = json.load(json_file)

    # Extraire les données
    csv_data = [extract_info(item) for item in data]

    # En-têtes du CSV
    csv_headers = ["URL", "Date", "Statut", "Erreurs", "Tests non-applicables", "Tests conformes",
                   "Tests non-conformes", "Tests pré-qualifiés", "Tests non-testés", "Note Asqatasun"]

    # Écrire dans un fichier CSV
    with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(csv_headers)
        csvwriter.writerows(csv_data)

    print(f"Conversion terminée. Le fichier CSV '{output_file}' a été créé.")


if __name__ == "__main__":
    main()
