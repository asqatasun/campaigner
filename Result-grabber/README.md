# Result Grabber

## 1. Export result with Asqatasun API

```shell
FIRST_SLICE=01
LAST_SLICE=36
for i in $(seq -f "%02g" ${FIRST_SLICE} ${LAST_SLICE}); do
  echo "========== ========== ========== ${i}"
  curl \
    -v \
    -u admin@asqatasun.org:myAsqaPassword \
    tiresias07.ovh.adullact.org:8080/api/v0/audit/tags/Campagne-2024-T2-tranche${i} \
    -o tranche${i}.json
done
```

## 2. Convert to CSV

```shell
python json_to_csv_converter.py tranche00.json tranche00.csv
```

```shell
FIRST_SLICE=01
LAST_SLICE=36
for i in $(seq -f "%02g" ${FIRST_SLICE} ${LAST_SLICE}); do
  echo "========== ========== ========== ${i}"
  python json_to_csv_converter.py \
    "/home/mfaure/Documents/Adullact/Dossier-2024/Projets/Observatoire/Campagne-2024-T2--2024-06-24/tranche${i}.json" \
    "/home/mfaure/Documents/Adullact/Dossier-2024/Projets/Observatoire/Campagne-2024-T2--2024-06-24/tranche${i}csv"
done
```