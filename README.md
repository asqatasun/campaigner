# Campaigner

## Description

Campaigner helps to launch lots of audits. It relies on the [PHP client of Asqatasun API](https://gitlab.com/asqatasun/api-client-php) 


## Authors and acknowledgment

Matthieu Faure

## License

Affero GPL v3+

## Project status

Project in development
