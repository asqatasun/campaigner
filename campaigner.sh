#!/usr/bin/env bash

set -o errexit

# "declare -r" = declare as readonly
declare -r URL_FILE_STUB="/home/people/mfaure/Observatoire-donnees/urls-tranche"
declare -r API_CLIENT_ROOT="/home/people/mfaure/api-client-php"
declare -r TAG_STUB="Campagne-2024-T2-tranche"
declare -r FIRST_SLICE="00"
declare -r LAST_SLICE="01"
declare -r AUDITS_PER_SLICE=1000

function wait_for_audits() {

  local THE_TAG="$1"
  local AUDIT_COUNT=0

  # Wait until 1000 audits ($AUDITS_PER_SLICE) are created for the given tag ($THE_TAG)
  until [ $AUDIT_COUNT == $AUDITS_PER_SLICE ]
  do
    sleep 1m
    echo -en "."
    AUDIT_COUNT=$(
      curl \
        --silent \
        -X 'GET' \
        'http://admin%40asqatasun.org:myAsqaPassword@localhost:8080/api/v0/audit/tags/'"${THE_TAG}" \
        -H 'accept: */*' \
      | jq -c '.[]' \
      | wc -l
      )
  done

}

function main() {

  local COUNTER
  local URL
  local MY_TAG
  local SLEEP_SECONDS=0
  local i

  for i in $(seq -f "%02g" ${FIRST_SLICE} ${LAST_SLICE}); do

    COUNTER=0

    # verbose
    echo "Slice ${i} ========================================================="

    MY_TAG="${TAG_STUB}${i}"

    # launch audits
    while IFS= read -r URL; do
      COUNTER=$(( COUNTER + 1 ));
      echo "$COUNTER $URL" ;
      "${API_CLIENT_ROOT}/api-client-php_sources/bin/console" asqatasun:page-audit-run -t "${MY_TAG}" "$URL";
      echo ;
      sleep "${SLEEP_SECONDS}s" ;
    done < "${URL_FILE_STUB}${i}"

    # Wait for the audits to complete, except for the last slice
    if [ "${i}" -lt ${LAST_SLICE} ]; then
      wait_for_audits "${MY_TAG}"
    fi

  done
}

# Run main
main